package org.wit.lists.main

import android.app.Application
import org.jetbrains.anko.AnkoLogger
import org.wit.lists.models.ListJSONStore
import org.wit.lists.models.ListStore

class MainApp : Application(), AnkoLogger {

    lateinit var lists: ListStore

    override fun onCreate() {
        super.onCreate()
        lists = ListJSONStore(applicationContext)
    }
}