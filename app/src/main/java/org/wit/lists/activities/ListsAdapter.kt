package org.wit.lists.activities

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.card_lists.view.*
import org.wit.lists.R
import org.wit.lists.helpers.readImageFromPath
import org.wit.lists.models.ListModel

interface ListListener {
    fun onListClick(list: ListModel)
}

class ListAdapter constructor(private var lists: List<ListModel>,
                                   private val listener: ListListener) : RecyclerView.Adapter<ListAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder {
        return MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_lists, parent, false))
    }

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        val list = lists[holder.adapterPosition]
        holder.bind(list, listener)
    }

    override fun getItemCount(): Int = lists.size

    class MainHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(list: ListModel,  listener : ListListener) {
            itemView.groceryName.text = list.title
            itemView.groceryPrice.text = list.description
            itemView.imageIcon.setImageBitmap(readImageFromPath(itemView.context, list.image))
            itemView.setOnClickListener { listener.onListClick(list) }
        }
    }
}