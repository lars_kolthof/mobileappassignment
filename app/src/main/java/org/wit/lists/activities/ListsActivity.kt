package org.wit.lists.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import kotlinx.android.synthetic.main.activity_lists.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult

import org.wit.lists.R
import org.wit.lists.main.MainApp
import org.wit.lists.models.ListModel

class ListsActivity : AppCompatActivity(), ListListener {

    lateinit var app: MainApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lists)
        app = application as MainApp

        //layout and populate for display
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager   //recyclerView is a widget in activity_lists.xml
        loadLists()

        //enable action bar and set title
        title = "Grocery Lists"
        toolbarMain.title = title
        setSupportActionBar(toolbarMain)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.item_add -> startActivityForResult<ListActivity>(200)
            R.id.item_map -> startActivity<ListsMapsActivity>()
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onListClick(list: ListModel) {
        startActivityForResult(intentFor<GroceryListActivity>().putExtra("check_groceries", list), 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        loadLists()
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun loadLists() {
        showLists(app.lists.findAll())
    }

    fun showLists (lists: List<ListModel>) {
        //recyclerView is a widget in activity_lists.xml
        recyclerView.adapter = ListAdapter(lists, this)
        recyclerView.adapter?.notifyDataSetChanged()
    }
}