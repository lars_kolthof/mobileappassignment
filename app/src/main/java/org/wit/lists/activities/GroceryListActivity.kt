package org.wit.lists.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_grocery_list.*
import org.jetbrains.anko.*
import org.wit.lists.R
import org.wit.lists.main.MainApp
import org.wit.lists.models.GroceryModel
import org.wit.lists.models.ListModel

class GroceryListActivity : AppCompatActivity(), GroceryListener, AnkoLogger {
    // Change the state of checked, and change background colours of the item cards.
    override fun onCheckBoxClick(grocery: GroceryModel, itemView : View) {
        grocery.checked = !grocery.checked
        if(grocery.checked){
            itemView.setBackgroundColor(Color.GRAY)
        }else{
            itemView.setBackgroundColor(Color.WHITE)
        }
        app.lists.update(list)
    }

    override fun onGroceryClick(grocery: GroceryModel) {
        startActivityForResult(intentFor<GroceryActivity>().putExtra("grocery_edit", grocery).putExtra("add_groceries", list), 0)
    }

    lateinit var app: MainApp
    var list = ListModel()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grocery_list)
        app = application as MainApp

        //layout and populate for display
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager   //recyclerView is a widget in activity_lists
        list = intent.extras.getParcelable<ListModel>("check_groceries")

        loadGrocery(list)

        //enable action bar and set title
        title = list.title
        toolbarMain.title = title
        setSupportActionBar(toolbarMain)

        sorting_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val groceries = list.groceryList
                when (p2) {
                    // Sort by price
                    1 -> groceries.sortedBy { x -> x.price }.forEach {
                        list.groceryList.remove(it)
                        list.groceryList.add(it)
                    }
                    // Sort by name
                    2 -> groceries.sortedBy { x -> x.name }.forEach {
                        list.groceryList.remove(it)
                        list.groceryList.add(it)
                    }
                    // Sort by newest entry
                    3 -> groceries.sortedByDescending { x -> x.id }.forEach {
                        list.groceryList.remove(it)
                        list.groceryList.add(it)
                    }
                    // Sort by it being checked or not
                    4 -> groceries.sortedBy { x -> x.checked }.forEach {
                        list.groceryList.remove(it)
                        list.groceryList.add(it)}
                }
                list.groceryList = groceries
                loadGrocery(list)
            }
        }
    }

    private fun loadGrocery(list : ListModel) {
        showGroceries(list.groceryList)
    }

    fun showGroceries (groceries: List<GroceryModel>) {
        //recyclerView is a widget in activity_lists.xml
        recyclerView.adapter = GroceryAdapter(groceries, this)
        recyclerView.adapter?.notifyDataSetChanged()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_grocery, menu)
        if (menu != null) menu.getItem(0).setVisible(true)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.item_cancel -> finish()
            R.id.btn_add_grocery -> onBtnAddClick()
            R.id.editList ->  startActivityForResult(intentFor<ListActivity>().putExtra("list_edit", list), 0)
            R.id.shareList -> shareList()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            // Check what activity based on intents and act accordingly.
            when {
                data!!.hasExtra("deleted") -> finish()
                data!!.hasExtra("editedLists") -> {
                    list = data!!.extras.getParcelable<ListModel>("editedLists")
                    loadGrocery(list)
                }
                else -> {
                    list = data!!.extras.getParcelable<ListModel>("newList")
                    loadGrocery(list)
                    super.onActivityResult(requestCode, resultCode, data)
                }
            }

        }
    }
    // Share a grocery list through a sharingIntent.
    private fun shareList(){
        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
        sharingIntent.type = "text/plain";
        var body =
                "Name: " + list.title + "\n" +
                "Description: " + list.description + "\n" +
                "Items: " + "\n"
        list.groceryList.forEach {
            body += "------ \n"
            body += "Item: " + it.name + "\n" +
                    "Price: " + it.price + "\n" +
                    "Amount: " + it.amount + "\n"
        }
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Grocery List")
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body)
        startActivity(Intent.createChooser(sharingIntent, "Share via"))
    }
    private fun onBtnAddClick(){
        startActivityForResult(intentFor<GroceryActivity>().putExtra("add_groceries", list), 0)
    }

}