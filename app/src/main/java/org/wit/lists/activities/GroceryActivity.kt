package org.wit.lists.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_grocery.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.toast
import org.wit.lists.R
import org.wit.lists.main.MainApp
import org.wit.lists.models.GroceryModel
import org.wit.lists.models.ListModel
import org.wit.lists.models.generateRandomId
import java.math.RoundingMode

class GroceryActivity : AppCompatActivity(), AnkoLogger {
    var list = ListModel()
    lateinit var app : MainApp
    var grocery = GroceryModel()
    var edit = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grocery)

        app = application as MainApp
        // Set the list the groceries have to be put into
        list = intent.extras.getParcelable<ListModel>("add_groceries")

        title = list.title
        toolbar.title = title
        setSupportActionBar(toolbar)
        // Set grocery to edit if edit button was pressed.
        if (intent.hasExtra("grocery_edit")) {
            grocery = intent.extras.getParcelable<GroceryModel>("grocery_edit")
            btnAddGrocery.setText(R.string.button_saveItem)
            groceryName.setText(grocery.name)
            groceryPrice.setText(grocery.price.toString())
            groceryAmount.setText(grocery.amount.toString())
            edit = true
        }

        btnAddGrocery.setOnClickListener {
            if(grocery == null){
                grocery = GroceryModel()
            }
            var error = ""
            if(groceryName.text.toString().length < 21) {
                grocery.name = groceryName.text.toString()
                if (!grocery.name.isNotEmpty()){
                    error = "Item name is empty, "
                }
            }else{
                error += "Name is too long, "
            }
            if(groceryAmount.text.toString().isNotEmpty()){
                if(groceryAmount.text.toString().length < 5){
                    grocery.amount = groceryAmount.text.toString().toInt()
                }else{
                    error += "amount is too high, "
                }
            }
            if(groceryPrice.text.toString().isNotEmpty()){
                if(groceryPrice.text.toString().length < 9){
                    grocery.price = groceryPrice.text.toString().toBigDecimal().setScale(2, RoundingMode.CEILING).toFloat()
                }else{
                    error += "price is too much, "
                }
            }
            // If there are not any errors, add/edit the item
            if (error.isEmpty()) {
                var listCopy = list.copy()
                var intent = Intent()
                if(edit){
                    var foundItem : GroceryModel? = listCopy.groceryList.find{ p -> p.id == grocery.id }
                    if(foundItem != null){
                        foundItem.name = grocery.name
                        foundItem.price = grocery.price
                        foundItem.amount = grocery.amount
                    }
                }else{
                    grocery.id = generateRandomId()
                    list.groceryList.add(grocery)
                }
                // Update current list with new or edited grocery items
                app.lists.update(listCopy)
                intent.putExtra("newList", listCopy)
                setResult(AppCompatActivity.RESULT_OK, intent)
                finish()
            } else {
                toast(error)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_grocery_activity, menu)
        if (menu != null) menu.getItem(0).isVisible = true
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.goBack -> finish()
            R.id.deleteItem -> {
                list.groceryList.remove(grocery)
                app.lists.update(list)
                // Refreshes GroceryListActivity and sets new list
                intent.putExtra("editedList", list)
                setResult(AppCompatActivity.RESULT_OK, intent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}