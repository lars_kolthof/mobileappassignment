package org.wit.lists.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.wit.lists.R
import org.wit.lists.helpers.readImage
import org.wit.lists.helpers.readImageFromPath
import org.wit.lists.helpers.showImagePicker
import org.wit.lists.main.MainApp
import org.wit.lists.models.Location
import org.wit.lists.models.ListModel
class ListActivity : AppCompatActivity(), AnkoLogger {

    var list = ListModel()
    lateinit var app : MainApp
    val IMAGE_REQUEST = 1
    val LOCATION_REQUEST = 2
    var edit = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        app = application as MainApp
        toolbarAdd.title = title
        setSupportActionBar(toolbarAdd)

        if (intent.hasExtra("list_edit")) {
            btnAdd.setText(R.string.button_saveList)
            chooseImage.setText(R.string.change_list_image)
            list = intent.extras.getParcelable<ListModel>("list_edit")
            listTitle.setText(list.title)
            listDescription.setText(list.description)
            listImage.setImageBitmap(readImageFromPath(this, list.image))
            edit = true
        }
        btnAdd.setOnClickListener() {
            list.title = listTitle.text.toString()
            list.description = listDescription.text.toString()
            if (list.title.isNotEmpty()) {
                // Add intent if its an edit.
                if(edit){
                    app.lists.update(list.copy())
                    intent.putExtra("editedLists", list)
                    setResult(AppCompatActivity.RESULT_OK, intent)
                }else{
                    app.lists.create(list.copy())
                    setResult(AppCompatActivity.RESULT_OK)
                }
                finish()
            }
            else {
                toast (R.string.message_noTitle)
            }
        }
        listLocation.setOnClickListener {
            val location = Location(52.245696, -7.139102, 15f)
            if (list.zoom != 0f) {
                location.lat =  list.lat
                location.lng = list.lng
                location.zoom = list.zoom
            }
            startActivityForResult(intentFor<MapsActivity>().putExtra("location", location), LOCATION_REQUEST)
        }
        chooseImage.setOnClickListener {
            showImagePicker(this, IMAGE_REQUEST)
        }

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_lists, menu)
        if (edit && menu != null) menu.getItem(0).setVisible(true)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            IMAGE_REQUEST -> {
                if (data != null) {
                    list.image = data.getData().toString()
                    listImage.setImageBitmap(readImage(this, resultCode, data))
                    chooseImage.setText(R.string.change_list_image)
                }
            }
            LOCATION_REQUEST -> {
                if (data != null) {
                    val location = data.extras.getParcelable<Location>("location")
                    list.lat = location.lat
                    list.lng = location.lng
                    list.zoom = location.zoom
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.item_cancel -> finish()
            R.id.item_delete -> {
                app.lists.delete(list)
                intent.putExtra("deleted", "adsfasdf")
                setResult(AppCompatActivity.RESULT_OK, intent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}