package org.wit.lists.activities

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.card_grocery.view.*
import org.wit.lists.R
import org.wit.lists.models.GroceryModel

interface GroceryListener {
    fun onGroceryClick(grocery: GroceryModel)
    fun onCheckBoxClick(grocery: GroceryModel, itemView : View)
}

class GroceryAdapter constructor(private var groceries: List<GroceryModel>,
                                   private val listener: GroceryListener) : RecyclerView.Adapter<GroceryAdapter.GroceryHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroceryHolder {
        return GroceryHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_grocery, parent, false))
    }

    override fun onBindViewHolder(holder: GroceryHolder, position: Int) {
        val grocery = groceries[holder.adapterPosition]
        holder.bind(grocery, listener)
    }

    override fun getItemCount(): Int = groceries.size

    class GroceryHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(grocery: GroceryModel,  listener : GroceryListener) {
            itemView.groceryName.text = grocery.name
            itemView.groceryPrice.text = grocery.price.toString()
            itemView.groceryAmount.text = grocery.amount.toString()
            itemView.setOnClickListener { listener.onGroceryClick(grocery) }
            // Set color and check checkbox depending on grocery item.
            itemView.checkBox.isChecked = grocery.checked
            if(grocery.checked){
                itemView.setBackgroundColor(Color.GRAY)
            }
            itemView.checkBox.setOnCheckedChangeListener{ _, _ -> listener.onCheckBoxClick(grocery, itemView) }


        }
    }
}