package org.wit.lists.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.AnkoLogger
import org.wit.lists.helpers.*
import java.util.*

val JSON_FILE = "lists.json"
val gsonBuilder = GsonBuilder().setPrettyPrinting().create()
val listType = object : TypeToken<java.util.ArrayList<ListModel>>() {}.type

fun generateRandomId(): Long {
    return Random().nextLong()
}

class ListJSONStore : ListStore, AnkoLogger {

    val context: Context
    var lists = mutableListOf<ListModel>()

    constructor (context: Context) {
        this.context = context
        if (exists(context, JSON_FILE)) {
            deserialize()
        }
    }

    override fun findAll(): MutableList<ListModel> {
        return lists
    }
    fun findGroceries(list : ListModel): ArrayList<GroceryModel>{
        return list.groceryList
    }
    override fun create(list: ListModel) {
        list.id = generateRandomId()
        lists.add(list)
        serialize()
    }

    override fun update(list: ListModel) {
        var foundList: ListModel? = lists.find { p -> p.id == list.id }
        if (foundList != null) {
            foundList.title = list.title
            foundList.description = list.description
            foundList.image = list.image
            foundList.lat = list.lat
            foundList.lng = list.lng
            foundList.zoom = list.zoom
            foundList.groceryList = list.groceryList
            serialize()
        }
    }

    override fun delete(list: ListModel){
        lists.remove(list)
        serialize()
    }

    private fun serialize() {
        val jsonString = gsonBuilder.toJson(lists, listType)
        write(context, JSON_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(context, JSON_FILE)
        lists = Gson().fromJson(jsonString, listType)
    }
}