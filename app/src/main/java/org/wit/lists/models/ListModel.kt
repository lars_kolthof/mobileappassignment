package org.wit.lists.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListModel(var id: Long = 0,
                          var title: String = "",
                          var image: String = "",
                          var description: String = "",
                          var lat : Double = 0.0,
                          var lng: Double = 0.0,
                          var zoom: Float = 0f,
                          var groceryList: ArrayList<GroceryModel> = arrayListOf() ) : Parcelable
@Parcelize
data class Location(var lat: Double = 0.0,
                    var lng: Double = 0.0,
                    var zoom: Float = 0f) : Parcelable