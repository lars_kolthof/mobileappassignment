package org.wit.lists.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GroceryModel(var id : Long = 0,
                        var name: String = "",
                        var price: Float = 0f,
                        var amount: Int = 0,
                        var checked: Boolean = false) : Parcelable