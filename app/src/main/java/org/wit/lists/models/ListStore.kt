package org.wit.lists.models

interface ListStore {
    fun findAll(): List<ListModel>
    fun create(list: ListModel)
    fun update(list: ListModel)
    fun delete(list: ListModel)
}